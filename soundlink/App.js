import 'react-native-gesture-handler';
import React from 'react';
import { View, Platform, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Button, Input, Text } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as ImagePicker from 'expo-image-picker';
import * as lzString from 'lz-string';
import API from './API/api';
import SoundComponent from './soundComponent';

const Stack = createStackNavigator();
const AuthContext = React.createContext();

function LoginScreen({ navigation }) {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  const { signIn } = React.useContext(AuthContext);

  return (
    <View>
      <Input
        placeholder="Email"
        value={email}
        onChangeText={setEmail}
      />
      <Input
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button
        icon={(
          <Icon
            name="sign-in"
            size={20}
            color="black"
          />
)}
        type="outline"
        raised={false}
        title="Sign in"
        onPress={() => signIn({ email, password, navigation })}
      />

      <Button
        style={{ paddingTop: '2%' }}
        icon={(
          <Icon
            name="sign-in"
            size={20}
            color="black"
          />
)}
        type="outline"
        raised={false}
        title="Register"
        onPress={() => navigation.navigate('Register')}
      />
    </View>
  );
}

function RegisterScreen({ navigation }) {
  const { register } = React.useContext(AuthContext);

  const [image, setImage] = React.useState(null);
  const [email, setEmail] = React.useState('');
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [password, setPassword] = React.useState('');

  React.useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  return (
    <View>
      <Input
        placeholder="First name"
        value={firstName}
        onChangeText={setFirstName}
      />
      <Input
        placeholder="Last name"
        value={lastName}
        onChangeText={setLastName}
      />
      <Input
        placeholder="Email"
        value={email}
        onChangeText={setEmail}
      />
      <Input
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button color="black" type="outline" title="Pick an image from camera roll" onPress={pickImage} />
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
      <Button
        style={{ paddingTop: '2%' }}
        icon={(
          <Icon
            name="sign-in"
            size={20}
            color="black"
          />
)}
        type="outline"
        raised={false}
        title="Register"
        onPress={() => register({
          firstName, lastName, email, password, image, navigation,
        })}
      />
      <Button
        style={{ paddingTop: '2%' }}
        icon={(
          <Icon
            name="sign-in"
            size={20}
            color="black"
          />
)}
        type="outline"
        raised={false}
        title="Back to sign in"
        onPress={() => navigation.navigate('Sign In')}
      />
    </View>
  );
}

function HomeScreen() {
  const { signOut } = React.useContext(AuthContext);
  const [firstName, setFirstName] = React.useState('');
  const [image, setImage] = React.useState('');

  React.useEffect(() => {
    AsyncStorage.getItem('userData').then((res) => {
      const restoredArray = JSON.parse(res);
      const decompressed = lzString.decompress(restoredArray.img.data);

      const img = `data:image/gif;base64,${decompressed}`;
      setImage(img);
      setFirstName(restoredArray.firstName);
    }).catch((err) => {
      console.log("error in setItem : 'userData'", err);
    });
  }, []);

  return (
    <View style={{ paddingTop: 50, alignItems: 'center' }}>
      <Text style={{ justifyContent: 'center' }}>
        Bonjour {' '}
        {firstName}
      </Text>
      <SoundComponent signOut={signOut}/>
      <Button style={{ paddingTop: '10%' }} title="Sign out" onPress={signOut} />

    </View>
  );
}

export default function App() {
  const api = new API();

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          AsyncStorage.setItem('userToken', JSON.stringify(action.payload.token)).then(() => {
            console.log("success setItem : 'userToken'");
            AsyncStorage.setItem('userData', JSON.stringify(action.payload.user)).then(() => {
              console.log("success setItem : 'userData'");
              action.payload.navigation.navigate('Home');
            }).catch((err) => {
              console.log("error in setItem : 'userData'", err);
            });
          }).catch((err) => {
            console.log("error in setItem : 'userToken'", err);
          });

          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          AsyncStorage.removeItem('userToken').then(() => {
            console.log("success removeItem : 'userToken'");
            AsyncStorage.removeItem('userData').then(() => {
              console.log("success removeItem : 'userData'");
            }).catch((err) => {
              console.log("error in removeItem : 'userData'", err);
            });
          }).catch((err) => {
            console.log("error in removeItem : 'userToken'", err);
          });
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
        case 'REGISTER':
          AsyncStorage.setItem('userToken', JSON.stringify(action.payload.token)).then(() => {
            console.log("success setItem : 'userToken'");
            AsyncStorage.setItem('userData', JSON.stringify(action.payload.user)).then(() => {
              console.log("success setItem : 'userData'");
              action.payload.navigation.navigate('Home');
            }).catch((err) => {
              console.log("error in setItem : 'userData'", err);
            });
          }).catch((err) => {
            console.log("error in setItem : 'userToken'", err);
          });
          return {
            ...prevState,
            isLoading: false,
            isSignout: false,
            userToken: action.payload.token,
            userData: action.payload.user,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    },
  );

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;


        userToken = await AsyncStorage.getItem('userToken').then(() => {
            dispatch({ type: 'RESTORE_TOKEN', token: userToken });
        }).catch(() => {
            dispatch({ type: 'RESTORE_TOKEN', token: userToken });
        });


      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
        dispatch({ type: 'RESTORE_TOKEN', token: userToken });

    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        try {
          const response = await api.login({
            email: data.email,
            password: data.password,
          });
          dispatch({ type: 'SIGN_IN', payload: { token: response.data.token, user: response.data.user, navigation: data.navigation } });
        } catch (err) {
          console.log("error");
          alert('Wrong credentials');
        }
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      register: async (data) => {
        try {
          //const base64String = data.image.split(',');
          //console.log(base64String[1]);
          const compressed = lzString.compress(data.image);
          console.log(compressed);
          const response = await api.register({
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: data.password,
            image: compressed,
          });

          console.log('navigation :', data.navigation);
          dispatch({ type: 'REGISTER', payload: { token: response.data.token, user: response.data.user, navigation: data.navigation } });
        } catch (err) {
            alert("- You need to fill all fields\n- Use valid email")
        }
      },
    }),
    [api],
  );

    return (
    <NavigationContainer>
      <AuthContext.Provider value={authContext}>
        <Stack.Navigator>
          {state.userToken !== null ? (
            <Stack.Screen name="Home" options={{ headerLeft: null,
                headerRight: () => (
                    <Button
                        onPress={() => authContext.signOut()}
                        title="Sign out!"
                        color="#ffff"
                    />
                ),
            }} component={HomeScreen} />
          ) : (
            <Stack.Screen name="Sign In" component={LoginScreen} />

          )}
          <Stack.Screen name="Register" options={{}} component={RegisterScreen} />
        </Stack.Navigator>
      </AuthContext.Provider>
    </NavigationContainer>
  );
}
