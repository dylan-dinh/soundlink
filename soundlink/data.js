export const data = [
    {
        name: "Neural Network",
        url: "https://soundcloud.com/kabayun/kabayun-ingrained-instincts-neural-network",
        artist: "Kabayun",
        feat: "Ingrained Instincts",
        genre: "Forest Psytrance"
    },
    {
        name: "Thunderstruck",
        url: "https://www.youtube.com/watch?v=v2AC41dglnM",
        artist: "AC/DC",
        genre: "Hard Rock"
    },
    {
        name: "Another One Bites The Dust",
        url: "https://www.youtube.com/watch?v=rY0WxgSXdEE",
        artist: "Queen",
        genre: "Rock'n'Roll"
    },
    {
        name: "Don't Stop Me Now",
        url: "https://www.youtube.com/watch?v=HgzGwKwLmgM",
        artist: "Queen",
        genre: "Rock'n'Roll"
    },
    {
        name: "Under Pressure",
        url: "https://www.youtube.com/watch?v=YoDh_gHDvkk",
        artist: "Queen",
        feat: "David Bowie",
        genre: "Rock'n'Roll"
    },
    {
        name: "All dogs go to heaven",
        artist: "$uicideBoy$",
        url: "https://www.youtube.com/watch?v=Rtbdgknf0AM",
        genre: "Underground US rap"
    },
    {
        name: "...And To Those I Love, Thanks For Sticking Around",
        artist: "$uicideBoy$",
        url: "https://www.youtube.com/watch?v=dq_SDNtWHDY",
        genre: "Underground US rap"
    },
    {
        name: "I NO LONGER FEAR THE RAZOR GUARDING MY HEEL I , II , III , IV",
        artist: "$uicideBoy$",
        url: "https://www.youtube.com/watch?v=n-E1gnMvzng",
        genre: "Underground US rap"
    },
    {
        "name": "One Last Look At The Damage",
        "artist": "$uicideBoy$",
        "url": "https://www.youtube.com/watch?v=pHv1y-BNs4k",
        "genre": "Underground US rap"
    },
    {
        "name": "NEW CHAINS, SAME SHACKLES",
        "artist": "$uicideBoy$",
        "url": "https://www.youtube.com/watch?v=yUqWw5Vx42g",
        "genre": "Underground US rap"
    },
    {
        "name": "Memoirs of a Gorilla",
        "artist": "$uicideBoy$",
        "url": "https://www.youtube.com/watch?v=B9eH5-g2JhU",
        "genre": "Underground US rap"
    },
    {
        "name": " That Just Isn't Empirically Possible",
        "artist": "$uicideBoy$",
        "url": "https://www.youtube.com/watch?v=kHQ8LJ0HGJU",
        "genre": "Underground US rap"
    },
    {
        "name": "KILL YOUR$ELF I , II , III , IV",
        "artist": "$uicideBoy$",
        "url": "https://www.youtube.com/watch?v=-T0VzbRENMU",
        "genre": "Underground US rap"
    },
    {
        "name": "Gatorade",
        "artist": "Yung Lean",
        "url": "https://www.youtube.com/watch?v=u24e43iW9KE",
        "genre": "Emo, cloud rap"
    },
    {
        "name": "Pikachu",
        "artist": "Yung Lean",
        "url": "https://www.youtube.com/watch?v=2IkulUI9llc",
        "genre": "Emo, cloud rap"
    },
    {
        "name": "Ginseng Strip 2002",
        "artist": "Yung Lean",
        "url": "https://www.youtube.com/watch?v=vrQWhFysPKY",
        "genre": "Emo, cloud rap"
    },
];