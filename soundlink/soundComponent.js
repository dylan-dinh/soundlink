import React from 'react';
import {View, Text, ScrollView, Linking} from 'react-native';
import {Card} from 'react-native-elements';
import styled from 'styled-components';
import { data } from './data';

const SoundComponent = () => (
    <ScrollView className="soundContainer">
      {
                        data.map((u, i) => (
                          <Card>
                            <Card.Title>{u.name}</Card.Title>
                            <Card.Divider />
                            <View key={i}>
                              <StyledText>
                                This song is made by
                                <Text style={{fontWeight:"bold"}}>{u.artist}</Text>
                                {' '}
                                <Text style={{fontStyle:"italic"}}>{u.feat && `feat ${u.feat}`}</Text>
                              </StyledText>
                              <StyledText>
                                Type:
                                <Text style={{fontWeight:"bold"}}>{u.genre}</Text>
                              </StyledText>
                              <StyledText>
                                To listen to this song, {' '}
                                <Text style={{fontStyle:"italic"}} onPress={() => Linking.openURL(u.url)}>
                                    click here
                                </Text>
                                {' '}
                                !
                              </StyledText>
                            </View>
                          </Card>
                        ))
                    }
    </ScrollView>
);

const StyledText = styled(Text)`
    justify-content: center;
`;

export default SoundComponent;
