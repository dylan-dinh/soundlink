import axios from "axios"
import defaults from "lodash/defaults"

class API {
    constructor(token) {
        this.token = token

        let credentials = {}

        const config = defaults(credentials, {
            headers:{
                'Access-Control-Allow-Origin' : '*',
                'Content-Type': 'application/json',
                'Authorization' : 'Bearer ' + token,
                'Accept': "application/json"
            }
        })
        this.axios = axios.create(config)

    }


    register = async (body) => {
        let response = await axios.post("https://soundlink-epitech.herokuapp.com/register", body,)
        console.log("response : ", response.data.user)
        return response
    }

    login = async (body) => {
        let response = await axios.post("https://soundlink-epitech.herokuapp.com/login", body,)
        console.log("response : ", response.data.user)
        return response


    }

}

export default API