function isEmptyObj(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            return
        }
    }
    throw new Error("Body is empty");
}

function isKeyFromObjIsEmpty(obj) {
    for (let key in obj) {
        if (obj[key] === "" || obj[key] === " ") {
            throw new Error("One value of key in Body is either '' or ' ' which is not a valid body");
        }
    }
}

// TODO: do it for number as well
let bodyStringChecker = async (req, res, next) => {
    try {
        isEmptyObj(req.body);
        isKeyFromObjIsEmpty(req.body);
        next()
    }
    catch (error) {
        res.status(500).send({error: "Body or one of its value is '' or ' ' "})
    }
};

module.exports = bodyStringChecker;
