let User = require('./../model/user');
const lzString = require('lz-string')

let createUser = async (req, res) => {
    try {
        const user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password,
            img: {data: req.body.image, contentType:"data:image/png;base64"}
        });
        await user.save();
        const token = await user.generateAuthToken();

        res.status(201).send({ user, token });
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    }
};

let login = async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.status(200).send({user, token})
    } catch (err) {
        res.status(400).send({
            error: "Votre email ou mot de passe est erroné.",
            info: err,
        })
    }
}

module.exports = {
    createUser,
    login,
}