const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const userSchema = mongoose.Schema({
    firstName: {
        type: String,
        trim: true,
    },
    lastName: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        /*validate: (value) => {
            if (!validator.isEmail(value)) {
                throw new Error("Invalid email address");
            }
        },*/
    },
    password: {
        type: String,
        required: true,
        minLength: 7,
    },
    img: {
        data: String,
        contentType: String
    },
    tokens: [
        {
            token: {
                type: String,
                required: true,
            },
            metadata: {
                type: Object,
            }
        },
    ],

})

userSchema.pre("save", async function (next) {
    // Hash the password before saving the user model
    const user = this;
    if (user.isModified("password")) {
        user.password = await bcrypt.hash(user.password, 8);
    }
    next();
});

userSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.
    const user = await User.findOne({email});
    if (!user) {
        throw new Error({error: "Invalid login credentials"});
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password);
    if (!isPasswordMatch) {
        throw new Error({error: "Invalid login credentials"});
    }
    return user;
};

userSchema.methods.generateAuthToken = async function (metadata) {
    // Generate an auth token for the user
    const user = this;
    const token = jwt.sign({_id: user._id}, 'HIGH_PRIVACY_KEY_');
    user.tokens = user.tokens.concat({token, metadata});
    await user.save();
    return token;
};


const User = mongoose.model("User", userSchema);

module.exports = User;
