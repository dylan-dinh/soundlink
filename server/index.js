const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const db = require("./db/db");
const MongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const userRouter = require('./router/user');

app.use(cors());
app.use(express.json());
app.use(bodyParser.raw());

app.use( bodyParser.json({limit: '50mb'}) );
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit:50000
}));


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Accept')
    next()
});

try {
    mongoose.connect(
        db.url,
        {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        },
        (err) => {
            if (err) return console.log(err);
            app.use("/", userRouter);
            app.get('/', (req, res) => {
                res.status(200).json({
                    message: "hello world",
                    success: true
                })
            });
            app.listen(process.env.PORT || 4000, () => {
                console.log("We are live on localhost:4000");
            });
            process.on("SIGINT", function() {
                console.log("Caught interrupt signal");
                process.exit();
            });
        }
    );
} catch (error) {
    console.log(error);
}