const router = require('express-promise-router')();
const userService = require('../service/user');
const auth = require('../middleware/auth');
const bodyChecker = require('../middleware/body_checker');


router.post("/register", bodyChecker ,userService.createUser);

router.post("/login", bodyChecker, userService.login);

module.exports = router;